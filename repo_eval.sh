#!/bin/bash

get_last_commit_sanitize() {
    local file="$1"
    local last_commit

    for commit in $(git --no-pager log --pretty="format:%H" "$file"); do
        if ! git --no-pager show --no-patch --pretty="format:%s%b" $commit | grep -iq "copyright"; then
            echo $commit
            return
        fi
    done
    # All seem to be about copyright, so just return the latest commit.
    # It is as good as any..
    echo $(git --no-pager log -1 --pretty="format:%H")
}

get_commit_date() {
    local commit="$1"
    local commit_date

    commit_date="$(git --no-pager show --no-patch --pretty="format:%ci" "$commit" | cut -d ' ' -f 1)"
    echo $commit_date
}

get_first_commit_date() {
    local rev
    local first_commit_date

    rev=$(git rev-list --max-parents=0 HEAD | tail -1)
    first_commit_date=$(get_commit_date $rev)
    echo $first_commit_date
}

get_loc() {
    local file="$1"
    local loc

    loc=$(cloc --csv $file | grep SUM | cut -d ',' -f 5)

    if test -z "$loc"; then
        loc=0
    fi

    echo $loc
}

get_file_properties_line() {
    local file="$1"
    local loc=0
    local last_commit
    local last_commit_date

    loc=$(get_loc "$file")
    last_commit=$(get_last_commit_sanitize "$file")
    last_commit_date=$(get_commit_date "$last_commit")
    printf "%s,%s,%s\n" "$last_commit_date" "$file" "$loc"
}
export -f get_last_commit_sanitize get_commit_date get_loc get_file_properties_line

# TODO: remove GCC specific paths and implement some generic handling
get_properties_list() {
    local type="$1"
    find . -not \( \
            -path ./contrib/\* -o \
            -path ./gcc/po/\* -o \
            -path ./gcc/testsuite/\* -o \
            -path ./libgomp/testsuite/\* -o \
            -path ./libphobos/testsuite/\* -o \
            -path ./libstdc++-v3/doc/\* -o \
            -path ./libstdc++-v3/testsuite/\* -o \
            -path ./zlib/\* -o \
            -path ./.git/\*  -prune \) -type $type -print0 | \
        xargs -0 -n 1 -P $(nproc) bash -c 'get_file_properties_line "$@"' _
}

get_file_properties_list() {
    get_properties_list "f"
}

get_dir_properties_list() {
    get_properties_list "d"
}

get_last_commit_date_avg() {
    local list="$1"
    local sum=0
    local cnt=0
    local avg=0

    for year in $(echo "$list" | cut -d '-' -f1); do
        sum=$((sum+year))
        cnt=$((cnt+1))
    done

    avg=$(echo "$sum/$cnt" | bc -l)
    echo $avg
}

get_last_commit_date_stdev() {
    local list="$1"
    local cnt=0
    local sum1=0
    local sum2=0
    local mean=0
    local stdev=0

    for year in $(echo "$list" | cut -d '-' -f1); do
        cnt=$((cnt+1))
        sum1=$((sum1+year))
    done
    mean=$(echo "$sum1 / $cnt" | bc -l)

    for year in $(echo "$list" | cut -d '-' -f1); do
        sum2=$(echo "$sum2 + (($year - $mean) * ($year - $mean))" | bc -l)
    done

    stdev=$(echo "sqrt($sum2/($cnt - 1))" | bc -l)
    echo $stdev
}

get_scored_properties_list() {
    local list="$1"
    local cnt=20
    test -z "$2" || cnt="$2"
    local year_now=$(date +%Y)
    local year_lst=0
    local year_dif=0
    local score=0
    local loc=0

    IFS_OLD="$IFS"
    IFS=$'\n'
    for line in $list; do
        year_lst=$(echo $line | cut -d '-' -f1)
        year_dif=$((year_now - year_lst))
        loc=$(echo $line | cut -d ',' -f3)
        score=$((year_dif * loc))
        echo $score,$line
    done
    IFS="$IFS_OLD"
}

get_nb_lines_unmaintained() {
    local list="$1"
    local limit="1000"
    test -z "$2" || limit="$2"
    local sum=0

    for line in $(echo "$list" | grep -v '\(configure\|\./intl\|test\)' | \
                                 grep -v '\.\(h\|d\|m4\|rst\|def\)\,' | \
                                 grep '^[0-9]\{4,\}\,'); do
       sum=$((sum + $(echo $line | cut -d ',' -f4)))
    done

    echo $sum
}

get_unmaintained_list() {
    local list="$1"
    local thresh=500
    test -z "$2" || thresh="$2"
    local score

    for line in $list; do
        score=$(echo "$line" | cut -d ',' -f 1)
        if test $score -gt $thresh; then
            echo "$line"
        fi
    done
}

dir_props_list=$(mktemp --tmpdir eval.dir.XXXX)
file_props_list=$(mktemp --tmpdir eval.file.XXXX)
scored_dir_props_list=$(mktemp --tmpdir eval.dir_scored.XXXX)
scored_file_props_list=$(mktemp --tmpdir eval.file_scored.XXXX)

printf "%s\n"    "Temporary files used for storing properties:"
printf "%s: %s\n" "Directory properties         " "$dir_props_list"
printf "%s: %s\n" "Directory properties (scored)" "$scored_dir_props_list"
printf "%s: %s\n" "File properties              " "$file_props_list"
printf "%s: %s\n" "File properties (scored)     " "$scored_file_props_list"
printf "\n"

get_dir_properties_list > $dir_props_list
get_file_properties_list > $file_props_list

get_scored_properties_list "$(cat $dir_props_list)" > $scored_dir_props_list
get_scored_properties_list "$(cat $file_props_list)" > $scored_file_props_list

first_commit=$(get_first_commit_date | cut -d '-' -f1)
last_commit_avg=$(get_last_commit_date_avg "$(cat $file_props_list)")
last_commit_stdev=$(get_last_commit_date_stdev "$(cat $file_props_list)")
nb_lines_total=$(get_loc .)
nb_lines_unmaintained=$(get_nb_lines_unmaintained "$(cat $scored_file_props_list)")
nb_contributors_total=$(git --no-pager shortlog -s -n --no-merges | wc -l)
nb_contributors_10=$(git --no-pager shortlog -s -n --no-merges | grep '^ *[0-9]\{2,\}' | wc -l)
nb_contributors_per_loc=$(echo "$nb_contributors_10 / $nb_lines_total" | bc -l)
dirs_unmaintained=$(get_unmaintained_list "$(cat $scored_dir_props_list)" | sort --numeric-sort --reverse)
files_unmaintained=$(get_unmaintained_list "$(cat $scored_file_props_list)" | sort --numeric-sort --reverse)

printf "%s %d\n"    "Year of first commit       :" $first_commit
printf "%s %7.2f\n" "Year of latest commit avg  :" $last_commit_avg
printf "%s %7.2f\n" "Year of latest commit stdev:" $last_commit_stdev
printf "\n"
printf "%s %8d\n"   "Total number of lines       :" $nb_lines_total
printf "%s %8d\n"   "Number of unmaintained lines:" $nb_lines_unmaintained
printf "%s %8.2f\n" "Percentage unmaintained     :" $(echo "100 * $nb_lines_unmaintained / $nb_lines_total" | bc -l)
printf "\n"
printf "%s %4d\n"   "Number of contributors total            :" $nb_contributors_total
printf "%s %4d\n"   "Number of contributors with >=10 commits:" $nb_contributors_10
printf "%s %4.2f\n" "Number of contributors per 1000 LOC     :" $(echo "1000 * $nb_contributors_per_loc" | bc -l)
printf "\n"
printf "%s\n"       "Potentially unmaintained directories:"
printf "%s\n"       "$dirs_unmaintained"
printf "\n"
printf "%s\n"       "Potentially unmaintained files:"
printf "%s\n"       "$files_unmaintained"
