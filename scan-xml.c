#include <stdio.h>
#include <string.h>

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

int find_string(char *string, size_t len, FILE *fp)
{
    int c;
    size_t nread;
    char buf[len];

    while ((c = getc(fp)) != EOF)
    {
        if (c == string[0])
        {
            ungetc(c, fp);
            nread = fread(buf, sizeof(*buf), ARRAY_SIZE(buf), fp);
            if (nread != ARRAY_SIZE(buf))
            {
                fprintf(stderr, "fread() failed: %zu\n", nread);
                return 1;
            }
            if (strstr(buf, string) == NULL)
            {
                continue;
            }
            else
            {
                return 0;
            }
        }
    }

    return 1;
}

void extract_string(char *start_string, char *end_string, FILE *fp)
{
    long start = 0;
    long end = 0;
    find_string(start_string, strlen(start_string), fp);
    start = ftell(fp);
    find_string(end_string, strlen(end_string), fp);
    end = ftell(fp) - strlen(end_string);

    fseek(fp, start, SEEK_SET);
    if (end - start > 0)
    {
            char buf[end-start+1];
            fread(buf, sizeof(*buf), ARRAY_SIZE(buf), fp);
            buf[end-start] = '\0';

            printf("%s", buf);
    }
}

int main(int argc, char* argv[])
{
    if (argc < 2)
    {
        printf("Usage: %s <file>\n", argv[0]);
        return 1;
    }

    char *file_name = argv[1];
    FILE *fp = NULL;


    fp = fopen(file_name, "rb");
    if (fp == NULL)
    {
        fprintf(stderr, "Could not open %s: ", file_name);
        perror("fopen");
        return 1;
    }

    int c; 
    while ((c = getc(fp)) != EOF)
    {
        ungetc(c, fp);
        if (find_string("Project Name", strlen("Project Name"), fp) == 0)
        {
            extract_string("<td>", "<", fp);
            printf(";");
        }
        if (find_string("Origin URL", strlen("Origin URL"), fp) == 0)
        {
            extract_string("href=\"", "\"", fp);
            printf(";");
        }
        if (find_string("issue tracking system?", strlen("issue tracking system?"), fp) == 0)
        {
            extract_string("href=\"", "\"", fp);
            printf("\n");
        }
    }

    fclose(fp);
    return 0;
}

